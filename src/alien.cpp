#include "alien.h"


alien::alien(sf::Vector2f Position, sf::Texture *txt)
{
    setTexture(*txt);
    setScale( 0.1, 0.1);
    location = Position;
    setPosition(location);
}

void alien::boom(shot *s)
{

    if(!(*s).fire)
    {
        sf::Vector2f v = {25,0};
        (*s).location = getPosition() + v;
        (*s).setPosition(location);
    }
    (*s).fire = true;

}
