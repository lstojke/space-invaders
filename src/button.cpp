#include "button.h"

button::button(std::string name)
{
    txt.loadFromFile(name);
    setTexture(txt);
    setPosition(world::WIDTH/2-150,world::HEIGHT/2-100);
    location = getPosition();
}
bool button::isClicked(world *w)
{
    location = getPosition();
    sf::Vector2i mousePosition = sf::Mouse::getPosition(*w);
    return  ( mousePosition.x > location.x ) && ( mousePosition.x < location.x + getTextureRect().width ) &&
            (  mousePosition.y > location.y ) && ( mousePosition.y < location.y + getTextureRect().height );
}

void button::boom(shot *s){}
button::~button()
{
    //dtor
}
