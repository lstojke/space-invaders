#include "shot.h"

shot::shot()
{
};

shot::shot(sf::Vector2f position, sf::Color c)
{
    txt.loadFromFile("shot.png");
    setTexture(txt);
    setColor(c);
    location = position;
    setPosition(location);
}

void shot::boom(shot *s)
{
     if( (getPosition().x <= (*s).getPosition().x+ shot::WIDTH) &&
       (getPosition().x + shot::WIDTH >= (*s).getPosition().x) &&
       (getPosition().y <= (*s).getPosition().y)  &&
       ((getPosition().y + shot::HEIGHT >= (*s).getPosition().y)) )
        {
            (*s).fire=false;
            fire=false;
            (*s).move(0,1000);
            move(0,-1000);
        }
}
