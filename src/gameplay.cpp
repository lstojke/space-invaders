#include "gameplay.h"

bool gameplay::mainMenu(world *game, bool *choice)
{
(*game).clear(sf::Color::Black);
forMenu.setCharacterSize(100);
forMenu.setPosition(world::WIDTH/4.0,world::HEIGHT/4.0);
forMenu.setString("SPACE INVADERS");
forMenu.setFillColor(sf::Color::Yellow);
(*game).draw(forMenu);
forMenu.setPosition(world::WIDTH/4.0,world::HEIGHT/2.0);
forMenu.setCharacterSize(50);
forMenu.setString("Press [ENTER] to play\nPress [ESC] to exit");
forMenu.setFillColor(sf::Color::White);
(*game).draw(s);
(*game).draw(e);
//(*game).draw(forMenu);
(*game).display();
if(sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) ||
   (sf::Mouse::isButtonPressed(sf::Mouse::Left) && s.isClicked(&(*game))))
    *choice = true;
if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && e.isClicked(&(*game)))
    (*game).close();
return *choice;
}

void gameplay::start(world *game)
{
(*game).clear(sf::Color::Black);
forMenu.setPosition(world::WIDTH/2-50,world::HEIGHT/3.0);
forMenu.setCharacterSize(300);
forMenu.setFillColor(sf::Color::White);
int i = 3;
while(i > 0){
(*game).clear(sf::Color::Black);
forMenu.setString(std::to_string(i));
i--;
(*game).draw(forMenu);
(*game).disp();
sf::sleep(sf::seconds(1));
}
}
void gameplay::play()
{
    world game;
    bool choice = false;
    bool started = false;
        while( game.isOpen() )
    {
        while( game.pollEvent( game.event ) )
        {
            if( game.event.type == sf::Event::Closed )
                 game.close();
            if( game.event.type == sf::Event::KeyPressed && game.event.key.code == sf::Keyboard::Escape )
                 game.close();
        }
        if(!choice)
        choice = mainMenu(&game, &choice);
        else
        {
            if(game.play.ifDraw())
            {
                if(!started){
                start(&game);
                started = true;
                }
                game.draaw();
            }
            else
            {
                while(!game.gameOver()){}
                    game.close();
                    gameplay::play();
                }
        }
    }
}
