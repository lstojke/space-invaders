#include "world.h"


world::world():
    forAliens("pink.jpg","box.png"),
    play(sf::Vector2f{world::WIDTH/2.0,world::HEIGHT-50}),
    inv(&forAliens),
    forScore("Space Cadets.ttf"),
    score(0),
    i_shot(0),
    lvl(1),
    l("Space Cadets.ttf"),
    GG("Space Cadets Italic.ttf")
    {
        shot temp(play.getPosition()+sf::Vector2f{25,0}, sf::Color::Magenta);
    for(int i=0; i<40; i++)
        m_shots[i] = enemyS[i] = temp;
    for(int i=0; i<3;i++)
    {
        live[i].setTexture(*play.getTexture());
        live[i].setScale(0.5,0.5);
        live[i].setPosition(30+40*(i+1),15);
    }
    l.setPosition(20,20);
    l.setCharacterSize(15);
    l.setString("LIVES: ");
    create(sf::VideoMode(WIDTH,HEIGHT,16),"Space Invaders - Lukasz Stojke");
    };
void world::restart()
{
    inv.restart(&forAliens);
}
int world::addScore(invaderManager *inv, shot *s)
{
    if((*inv).kill(s))
        return 4;
    else return 0;
}

void world::showScore()
{
    forScore.setPosition(world::WIDTH-200,20);
    forScore.setString("YOUR SCORE : " + std::to_string(score));
    forScore.setCharacterSize(15);
    forScore.setFillColor(sf::Color::White);
}
bool world::gameOver()
{
    clear(sf::Color::Black);
    for(int i=0; i<inv.getSize();  i++)
            draw(inv.m_aliens[i]);
    draw(play);
    for(int i=0; i<40; i++)
        if(enemyS[i].fire)
            draw(enemyS[i]);
    draw(forScore);
    GG.setPosition(world::WIDTH/2.0-400,world::HEIGHT/2.0);
    GG.setString("Game over !\nYour score: " + std::to_string(score) + "\nPRESS [ENTER] TO GO TO MAIN MENU");
    GG.setCharacterSize(50);
    GG.setFillColor(sf::Color::White);
    draw(GG);
    display();
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
       return true;
    return false;
}


void world::draaw()
{
    setFramerateLimit(60);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        play.move(-10,0);
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        play.move(10,0);
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
    {
        const sf::Time TIME_GAP = sf::seconds(0.25);
        if(cl3.getElapsedTime() > TIME_GAP){
        m_shots[i_shot].setColor(sf::Color::Cyan);
        play.boom(&m_shots[i_shot]);
        i_shot==40 ? i_shot=0 : i_shot++;
        cl3.restart();}
    }
    clear(sf::Color::Black);
    for(int i=0; i<40; i++)
    {
        if(m_shots[i].fire){
                m_shots[i].move(0,-15);
        draw(m_shots[i]);
        score += addScore(&inv,&m_shots[i]);}
        std::vector<alien>::iterator al = inv.m_aliens.begin();
        alien b = *al;
        if(m_shots[i].getlocation().y < b.getPosition().y){
            m_shots[i].fire = false;
            m_shots[i].move(0,-1000);}
        for(int j=0; j<40; j++)
        m_shots[i].boom(&enemyS[j]);
    }
    for(int i=0; i<40; i++)
    if(enemyS[i].fire)
    {
        enemyS[i].move(0,15);
        draw(enemyS[i]);
        if(enemyS[i].getlocation().y > world::HEIGHT)
        {
            enemyS[i].fire = false;
            enemyS[i].move(0,100);
        }
    }
    for(int i=0; i<lvl; i++)
    {
    inv.attack(&cl2, &enemyS[i]);
    i_shot==40 ? i_shot=0 : i_shot++;
    play.kill(&enemyS[i]);
    }
    inv.moveAliens(&cl);
    disp();
}


void world::disp()
{
    showScore();
    draw(forScore);
    draw(l);
    for(int i=0; i<play.lives;i++)
        draw(live[i]);
    if(inv.getSize()==0)
        restart();
    for(int i=0; i<inv.getSize();  i++)
        draw(inv.m_aliens[i]);
    if(play.ifDraw())
    draw(play);
    display();
}


