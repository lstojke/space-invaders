#include "invaderManager.h"
#include "alien.h"
#include "world.h"
#include "shot.h"

bool invaderManager::bam(character *a)
{
        if((*a).getPosition().x+alien::WIDTH >= world::WIDTH -15 || (*a).getPosition().x < 15)
        return true;
    return false;

}

bool invaderManager::kill(shot *s)
{
    int i=0;
    for(auto& a : m_aliens)
    {
    if( (a.getPosition().x <= (*s).getPosition().x+ shot::WIDTH) &&
       (a.getPosition().x + alien::WIDTH >= (*s).getPosition().x) &&
       (a.getPosition().y <= (*s).getPosition().y)  &&
       ((a.getPosition().y + alien::HEIGHT >= (*s).getPosition().y)))
        {
        m_aliens.erase(m_aliens.begin()+i);
        (*s).fire = false;
        (*s).setPosition(0,500);
        return true;
        }
        i++;
    }
        return false;
}

invaderManager::invaderManager(resources *res) : lvl(1)
{
    const int GAP = 10;
    for(int i=0; i<5; i++)
        for(int j=0; j<11; j++)
        {
            float alienX = j * alien::WIDTH + (GAP  * j * 3) + alien::WIDTH;
            float alienY = i * (alien::HEIGHT+10) + (GAP * i) + alien::HEIGHT+30 ;
            if(i%2==0)
            m_aliens.emplace_back(sf::Vector2f{alienX,alienY}, &(res->txt));
            else
            m_aliens.emplace_back(sf::Vector2f{alienX,alienY}, &(res->txt2));
        }
}

void invaderManager::restart(resources *res)
{
    lvl++;
    const int GAP = 10;
    for(int i=0; i<5; i++)
        for(int j=0; j<11; j++)
        {
            float alienX = j * alien::WIDTH + (GAP  * j * 3) + alien::WIDTH;
            float alienY = i * (alien::HEIGHT+10) + (GAP * i) + alien::HEIGHT+30 ;
            if(i%2==0)
            m_aliens.emplace_back(sf::Vector2f{alienX,alienY}, &(res->txt));
            else
            m_aliens.emplace_back(sf::Vector2f{alienX,alienY}, &(res->txt2));
        }
}


void invaderManager::moveAliens(sf::Clock *cl)
{

    const sf::Time TIME_GAP = sf::seconds(0.25);
    if((*cl).getElapsedTime() > TIME_GAP)
    {
        bool stepDown = false;
        double v = movingRight ? 10.0f : -10.0f;
        if(m_stepDown)
            v*=-1;
        int i=0;
        for(auto& al : m_aliens)
        {
                al.move(v,0);
            if(m_stepDown)
                al.move(0,alien::HEIGHT);
            else if(!stepDown)
                stepDown = bam(&al);
            i++;
        }

    if(m_stepDown) movingRight = !movingRight;
    m_stepDown = stepDown;
    (*cl).restart();
    }
}



void invaderManager::attack(sf::Clock *cl, shot *s)
{
    const sf::Time TIME_GAP = sf::seconds(1.5/(2*lvl));
    if((*cl).getElapsedTime() > TIME_GAP)
    {
        for(auto& al : m_aliens)
        {
            int random = rand()%8;
            bool ifAdd = true;
            for(auto& al2 : m_aliens)
            {
                if( (al.getPosition().x == al2.getPosition().x) &&
                    (al.getPosition().y <  al2.getPosition().y) )
                    ifAdd = false;
            }
            if(ifAdd && random == 1)
            {
                al.boom(s);
                (*cl).restart();
            }
        }

    }
}
