#include "tester.h"
#include <cassert>
tester::tester()
{
    //ctor
}

tester::~tester()
{
    //dtor
}
void tester::alienSize()
{
    assert(w.inv.getSize()==55);
}
void tester::fontLoaded()
{
    assert(w.l.getFont() != NULL);
    assert(w.forScore.getFont()!=NULL);
    assert(w.GG.getFont() != NULL);
}
void tester::resourceCheck()
{
    assert(w.forAliens.txt.getSize().x!=0);
    assert(w.forAliens.txt2.getSize().x!=0);
}
void tester::scoreCheck()
{
    assert(w.score==0);
    assert(w.play.lives>0);
}
void tester::ifReady()
{
    alienSize();
    fontLoaded();
    resourceCheck();
    scoreCheck();
}

