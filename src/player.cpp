#include "player.h"
#include "world.h"
player::player(sf::Vector2f Position) : lives(3)
{
    txt.loadFromFile("Player.png");
    setTexture(txt);
    location = Position;
    setPosition(location);
}
void player::move(float x, float y)
{
    if(( border() && goingRight && x > 0 ) ||
     (border() && !goingRight && x < 0))
        return;
    if(( goingRight && x < 0) ||
      (!goingRight && x > 0))
      goingRight = !goingRight;
    location += {x,y};
    setPosition(location);
}

void player::boom(shot *s)
{
    if(!(*s).fire)
    {
        sf::Vector2f v = {25+ getPosition().x, getPosition().y};
        (*s).location =  v  ;
        (*s).setPosition(location);
    }
    (*s).fire = true;
}

bool player::border()
{
    if(getPosition().x+character::WIDTH >= world::WIDTH -15 || getPosition().x < 15)
        return true;
    return false;
}

void player::kill(shot *s)
{
    if( (getPosition().x <= (*s).getPosition().x+ shot::WIDTH) &&
       (getPosition().x + character::WIDTH >= (*s).getPosition().x) &&
       (getPosition().y <= (*s).getPosition().y)  &&
       ((getPosition().y + character::HEIGHT >= (*s).getPosition().y )))
        {
        (*s).fire = false;
        (*s).move(0,100);
        lives--;
        }
    if(lives==0)
        toDraw = false;
}

