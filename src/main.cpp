#include <SFML/Graphics.hpp>
#include "gameplay.h"
#include "tester.h"
#include <ctime>
int main()
{
    tester test;
    test.ifReady();
    srand(time(NULL));
    gameplay game;
    game.play();
    return 0;
}
