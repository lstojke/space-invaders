#ifndef CHARACTER_H
#define CHARACTER_H


#include <SFML/Graphics.hpp>
class shot;
class character : public sf::Sprite
{
protected:
    sf::Texture txt;
    sf::Vector2f location;

public:
    void move(float x, float y);
    virtual void boom(shot *s)=0;
    constexpr static double WIDTH = 55;
    constexpr static double HEIGHT = 40;
};
#endif // CHARACTER_H
