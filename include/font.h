#ifndef FONT_H
#define FONT_H

class font : public sf::Text
{
    sf::Font fnt;
public:

    font(std::string name) { fnt.loadFromFile(name); setFont(fnt); };
};
#endif // FONT_H
