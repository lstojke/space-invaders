#ifndef ALIEN_H
#define ALIEN_H

#include <SFML/Graphics.hpp>
#include "resources.h"
#include "character.h"
#include "shot.h"
class alien : public character
{

public:
    alien(sf::Vector2f Position, sf::Texture *txt);

    void boom(shot *s);
};
#endif // ALIEN_H
