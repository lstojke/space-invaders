#ifndef PLAYER_H
#define PLAYER_H


#include <SFML/Graphics.hpp>
#include "character.h"
#include "shot.h"
class world;
class player : public character
{
    bool goingRight = false;
    bool toDraw = true;
    int lives;
public:
    player(sf::Vector2f Position);
    void move(float x, float y);
    bool border();
    void boom(shot *s);
    void kill(shot *s);
    bool ifDraw() { return toDraw; };
    friend class world;
    friend class tester;
};
#endif // CHARACTER_H

