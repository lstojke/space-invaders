#ifndef GAMPELAY_H
#define GAMEPLAY_H

#include "world.h"
#include "button.h"
class gameplay
{
    button s,e;
    font forMenu;
    int lvl;
public:
    gameplay() :  s("Button.png"), e("Exit.png"), forMenu("QuirkyRobot.ttf")
    {
        e.setPosition(world::WIDTH/2-150,world::HEIGHT/2+50);
    };
    world restart();
    bool mainMenu(world *game, bool *choice);
    void start(world *game);
    void play();
    void gameOver();

};
#endif // GAMPELAY_H
