#ifndef INVADERMANAGER_H
#define INVADERMANAGER_H

#include "alien.h"
#include "shot.h"

class invaderManager
{
    std::vector<alien> m_aliens;
    bool m_stepDown = false;
    bool movingRight = true;
    int lvl;
public:
    invaderManager(resources *res);
    bool bam(character *a);
    void moveAliens(sf::Clock *cl);
    bool kill(shot *s);
    int getSize() {return m_aliens.size();};
    void attack(sf::Clock *cl, shot *s);
    void restart(resources *res);
    friend class world;
};


#endif
