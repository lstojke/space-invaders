#ifndef SHOT_H
#define SHOT_H

#include <iostream>
#include "character.h"
//#include "player.h"

class shot : public character
{
public:
    shot();
    shot(sf::Vector2f position, sf::Color c);
    constexpr static double WIDTH = 5;
    constexpr static double HEIGHT = 30;
    bool fire = false;
    sf::Vector2f getlocation() { return location; };
    void attack();
    friend class player;
    friend class alien;
    void boom(shot *s);
};


#endif // SHOT_H
