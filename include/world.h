#ifndef WORLD_H
#define WORLD_H

#include "alien.h"
#include "InvaderManager.h"
#include <vector>
#include "character.h"
#include "player.h"
#include "shot.h"
#include "resources.h"
#include "font.h"

class world : public sf::RenderWindow
{
    resources forAliens;
    player play;
    invaderManager inv;
    sf::Clock cl,cl2,cl3;
    shot enemyS[40];
    shot m_shots[40];
    font forScore;
    sf::Event event;
    int score;
    int i_shot;
    int lvl;
    sf::Sprite live[3];
    font l;
    font GG;

public:
    constexpr static int WIDTH = 1200;
    constexpr static int HEIGHT = 900;
    world();
    void draaw();
    int addScore(invaderManager *inv, shot *s);
    void restart();
    void showScore();
    bool gameOver();
    void disp();
    friend class gameplay;
    friend class tester;
};

#endif // WORLD_H
