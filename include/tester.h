#ifndef TESTER_H
#define TESTER_H

#include "../world.h"
class tester
{
    world w;
    public:
        tester();
        ~tester();
        void fontLoaded();
        void alienSize();
        void resourceCheck();
        void scoreCheck();
        void ifReady();

};

#endif // TESTER_H
