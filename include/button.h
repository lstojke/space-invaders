#ifndef BUTTON_H
#define BUTTON_H

#include "character.h"
#include "world.h"

class button : public character
{
    public:
        button(std::string name);
        virtual ~button();
        bool isClicked(world *w);
        void boom(shot *s);
    protected:

    private:
};

#endif // BUTTON_H
