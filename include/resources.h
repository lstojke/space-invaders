#ifndef RESOURCES_H
#define RESOURCES_H

#include "font.h"
class resources
{
public:
    sf::Texture txt,txt2;
    font fnt;
    resources() : fnt("Space Cadets.ttf"){};
    resources(std::string name, std::string name2) : fnt("Space Cadets.ttf") { txt.loadFromFile(name); txt2.loadFromFile(name2); };
};
#endif // RESOURCES_H
